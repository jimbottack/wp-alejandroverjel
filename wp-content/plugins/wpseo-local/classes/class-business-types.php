<?php
/**
 * @package WPSEO_Local\Main
 * @since   7.7
 */

if ( ! class_exists( 'WPSEO_Local_Business_Types_Repository' ) ) {

	/**
	 * WPSEO_Local_Business_Types_Repository class. Handles all basic needs for the plugin, like custom post_type/taxonomy.
	 */
	class WPSEO_Local_Business_Types_Repository {

		/**
		 * An array of business types.
		 *
		 * @var array
		 */
		public $business_types = array();


		/**
		 * A flattened array of all business types for usage in <select> boxes
		 *
		 * @var array
		 */
		private $flattened_business_types = array();

		/**
		 * Constructor for the WPSEO_Local_Core class.
		 *
		 * @since 7.7
		 */
		public function __construct() {
			$this->setup();
		}

		/**
		 * Setup functionality and hooks for this class.
		 *
		 * @since 7.7
		 */
		public function setup() {
			$this->set_business_types();
		}

		/**
		 * Add predefined business types to a filterable array.
		 *
		 * @since 7.7
		 */
		private function set_business_types() {
			$business_types = array(
				'Organization' => array(
					'label'    => __( 'Organization', 'yoast-local-seo' ),
					'children' => array(
						'Airline'                 => __( 'Airline', 'yoast-local-seo' ),
						'Consortium'              => __( 'Consortium', 'yoast-local-seo' ),
						'Corporation'             => __( 'Corporation', 'yoast-local-seo' ),
						'EducationalOrganization' => array(
							'label'    => __( 'Educational organization', 'yoast-local-seo' ),
							'children' => array(
								'CollegeOrUniversity' => __( 'College or university', 'yoast-local-seo' ),
								'ElementarySchool'    => __( 'Elementary school', 'yoast-local-seo' ),
								'HighSchool'          => __( 'High school', 'yoast-local-seo' ),
								'MiddleSchool'        => __( 'Middle school', 'yoast-local-seo' ),
								'Preschool'           => __( 'Preschool', 'yoast-local-seo' ),
								'School'              => __( 'School', 'yoast-local-seo' ),
							),
						),
						'FundingScheme'           => __( 'Funding scheme', 'yoast-local-seo' ),
						'GovernmentOrganization'  => __( 'Government organization', 'yoast-local-seo' ),
						'LibrarySystem'           => __( 'Library system', 'yoast-local-seo' ),
						'LocalBusiness'           => array(
							'label'    => __( 'Local business', 'yoast-local-seo' ),
							'children' => array(
								'AnimalShelter'               => __( 'Animal shelter', 'yoast-local-seo' ),
								'ArchiveOrganization'         => __( 'Archive organization', 'yoast-local-seo' ),
								'AutomotiveBusiness'          => array(
									'label'    => __( 'Automotive business', 'yoast-local-seo' ),
									'children' => array(
										'AutoBodyShop'     => __( 'Auto body shop', 'yoast-local-seo' ),
										'AutoDealer'       => __( 'Auto dealer', 'yoast-local-seo' ),
										'AutoPartsStore'   => __( 'Auto parts store', 'yoast-local-seo' ),
										'AutoRental'       => __( 'Auto rental', 'yoast-local-seo' ),
										'AutoRepair'       => __( 'Auto repair', 'yoast-local-seo' ),
										'AutoWash'         => __( 'Auto wash', 'yoast-local-seo' ),
										'GasStation'       => __( 'Gas station', 'yoast-local-seo' ),
										'MotorcycleDealer' => __( 'Motorcycle dealer', 'yoast-local-seo' ),
										'MotorcycleRepair' => __( 'Motorcycle repair', 'yoast-local-seo' ),
									),
								),
								'ChildCare'                   => __( 'Child care', 'yoast-local-seo' ),
								'Dentist'                     => __( 'Dentist', 'yoast-local-seo' ),
								'DryCleaningOrLaundry'        => __( 'Dry cleaning or laundry', 'yoast-local-seo' ),
								'EmergencyService'            => array(
									'label'    => __( 'Emergency service', 'yoast-local-seo' ),
									'children' => array(
										'FireStation'   => __( 'Fire station', 'yoast-local-seo' ),
										'Hospital'      => __( 'Hospital', 'yoast-local-seo' ),
										'PoliceStation' => __( 'Police station', 'yoast-local-seo' ),
									),
								),
								'EmploymentAgency'            => __( 'Employment agency', 'yoast-local-seo' ),
								'EntertainmentBusiness'       => array(
									'label'    => __( 'Entertainment business', 'yoast-local-seo' ),
									'children' => array(
										'AdultEntertainment' => __( 'Adult entertainment', 'yoast-local-seo' ),
										'AmusementPark'      => __( 'Amusement park', 'yoast-local-seo' ),
										'ArtGallery'         => __( 'Art gallery', 'yoast-local-seo' ),
										'Casino'             => __( 'Casino', 'yoast-local-seo' ),
										'ComedyClub'         => __( 'Comedy club', 'yoast-local-seo' ),
										'MovieTheater'       => __( 'Movie theater', 'yoast-local-seo' ),
										'NightClub'          => __( 'Night club', 'yoast-local-seo' ),
									),
								),
								'FinancialService'            => array(
									'label'    => __( 'Financial service', 'yoast-local-seo' ),
									'children' => array(
										'AccountingService' => __( 'Accounting service', 'yoast-local-seo' ),
										'AutomatedTeller'   => __( 'Automated teller', 'yoast-local-seo' ),
										'BankOrCreditUnion' => __( 'Bank or credit union', 'yoast-local-seo' ),
										'InsuranceAgency'   => __( 'Insurance agency', 'yoast-local-seo' ),
									),
								),
								'FoodEstablishment'           => array(
									'label'    => __( 'Food establishment', 'yoast-local-seo' ),
									'children' => array(
										'Bakery'             => __( 'Bakery', 'yoast-local-seo' ),
										'BarOrPub'           => __( 'Bar or pub', 'yoast-local-seo' ),
										'Brewery'            => __( 'Brewery', 'yoast-local-seo' ),
										'CafeOrCoffeeShop'   => __( 'Cafe or coffee shop', 'yoast-local-seo' ),
										'Distillery'         => __( 'Distillery', 'yoast-local-seo' ),
										'FastFoodRestaurant' => __( 'Fast food restaurant', 'yoast-local-seo' ),
										'IceCreamShop'       => __( 'Ice cream shop', 'yoast-local-seo' ),
										'Restaurant'         => __( 'Restaurant', 'yoast-local-seo' ),
										'Winery'             => __( 'Winery', 'yoast-local-seo' ),
									),
								),
								'GovernmentOffice'            => array(
									'label'    => __( 'Government office', 'yoast-local-seo' ),
									'children' => array(
										'PostOffice' => __( 'Post office', 'yoast-local-seo' ),
									),
								),
								'HealthAndBeautyBusiness'     => array(
									'label'    => __( 'Health and beauty business', 'yoast-local-seo' ),
									'children' => array(
										'BeautySalon'  => __( 'Beauty salon', 'yoast-local-seo' ),
										'DaySpa'       => __( 'Day spa', 'yoast-local-seo' ),
										'HairSalon'    => __( 'Hair salon', 'yoast-local-seo' ),
										'HealthClub'   => __( 'Health club', 'yoast-local-seo' ),
										'NailSalon'    => __( 'Nail salon', 'yoast-local-seo' ),
										'TattooParlor' => __( 'Tattoo parlor', 'yoast-local-seo' ),
									),
								),
								'HomeAndConstructionBusiness' => array(
									'label'    => __( 'Home and construction business', 'yoast-local-seo' ),
									'children' => array(
										'Electrician'       => __( 'Electrician', 'yoast-local-seo' ),
										'GeneralContractor' => __( 'General contractor', 'yoast-local-seo' ),
										'HVACBusiness'      => __( 'HVAC business', 'yoast-local-seo' ),
										'HousePainter'      => __( 'House painter', 'yoast-local-seo' ),
										'Locksmith'         => __( 'Locksmith', 'yoast-local-seo' ),
										'MovingCompany'     => __( 'Moving company', 'yoast-local-seo' ),
										'Plumber'           => __( 'Plumber', 'yoast-local-seo' ),
										'RoofingContractor' => __( 'Roofing contractor', 'yoast-local-seo' ),
									),
								),
								'InternetCafe'                => __( 'Internet caf&eacute;', 'yoast-local-seo' ),
								'LegalService'                => array(
									'label'    => __( 'Legal service', 'yoast-local-seo' ),
									'children' => array(
										'Attorney' => __( 'Attorney', 'yoast-local-seo' ),
										'Notary'   => __( 'Notary', 'yoast-local-seo' ),
									),
								),
								'Library'                     => __( 'Library', 'yoast-local-seo' ),
								'LodgingBusiness'             => array(
									'label'    => __( 'Lodging business', 'yoast-local-seo' ),
									'children' => array(
										'BedAndBreakfast' => __( 'Bed and breakfast', 'yoast-local-seo' ),
										'Campground'      => __( 'Campground', 'yoast-local-seo' ),
										'Hostel'          => __( 'Hostel', 'yoast-local-seo' ),
										'Hotel'           => __( 'Hotel', 'yoast-local-seo' ),
										'Motel'           => __( 'Motel', 'yoast-local-seo' ),
										'Resort'          => __( 'Resort', 'yoast-local-seo' ),
									),
								),
								'MedicalBusiness'             => array(
									'label'    => __( 'Medical business', 'yoast-local-seo' ),
									'children' => array(
										'CommunityHealth' => __( 'Community health', 'yoast-local-seo' ),
										'Dentist'         => __( 'Dentist', 'yoast-local-seo' ),
										'Dermatology'     => __( 'Dermatology', 'yoast-local-seo' ),
										'DietNutrition'   => __( 'Diet nutrition', 'yoast-local-seo' ),
										'Emergency'       => __( 'Emergency', 'yoast-local-seo' ),
										'Geriatric'       => __( 'Geriatric', 'yoast-local-seo' ),
										'Gynecologic'     => __( 'Gynecologic', 'yoast-local-seo' ),
										'MedicalClinic'   => __( 'Medical clinic', 'yoast-local-seo' ),
										'Midwifery'       => __( 'Midwifery', 'yoast-local-seo' ),
										'Nursing'         => __( 'Nursing', 'yoast-local-seo' ),
										'Obstetric'       => __( 'Obstetric', 'yoast-local-seo' ),
										'Oncologic'       => __( 'Oncologic', 'yoast-local-seo' ),
										'Optician'        => __( 'Optician', 'yoast-local-seo' ),
										'Optometric'      => __( 'Optometric', 'yoast-local-seo' ),
										'Otolaryngologic' => __( 'Otolaryngologic', 'yoast-local-seo' ),
										'Pediatric'       => __( 'Pediatric', 'yoast-local-seo' ),
										'Pharmacy'        => __( 'Pharmacy', 'yoast-local-seo' ),
										'Physician'       => __( 'Physician', 'yoast-local-seo' ),
										'Physiotherapy'   => __( 'Physiotherapy', 'yoast-local-seo' ),
										'PlasticSurgery'  => __( 'Plastic surgery', 'yoast-local-seo' ),
										'Podiatric'       => __( 'Podiatric', 'yoast-local-seo' ),
										'PrimaryCare'     => __( 'Primary care', 'yoast-local-seo' ),
										'Psychiatric'     => __( 'Psychiatric', 'yoast-local-seo' ),
										'PublicHealth'    => __( 'Public health', 'yoast-local-seo' ),
									),
								),
								'ProfessionalService'         => __( 'Professional service', 'yoast-local-seo' ),
								'RadioStation'                => __( 'Radio station', 'yoast-local-seo' ),
								'RealEstateAgent'             => __( 'Real estate agent', 'yoast-local-seo' ),
								'RecyclingCenter'             => __( 'Recycling center', 'yoast-local-seo' ),
								'SelfStorage'                 => __( 'Self storage', 'yoast-local-seo' ),
								'ShoppingCenter'              => __( 'Shopping center', 'yoast-local-seo' ),
								'SportsActivityLocation'      => array(
									'label'    => __( 'Sports activity location', 'yoast-local-seo' ),
									'children' => array(
										'BowlingAlley'       => __( 'Bowling alley', 'yoast-local-seo' ),
										'ExerciseGym'        => __( 'Exercise gym', 'yoast-local-seo' ),
										'GolfCourse'         => __( 'Golf course', 'yoast-local-seo' ),
										'HealthClub'         => __( 'Health club', 'yoast-local-seo' ),
										'PublicSwimmingPool' => __( 'Public swimming pool', 'yoast-local-seo' ),
										'SkiResort'          => __( 'Ski resort', 'yoast-local-seo' ),
										'SportsClub'         => __( 'Sports club', 'yoast-local-seo' ),
										'StadiumOrArena'     => __( 'Stadium or arena', 'yoast-local-seo' ),
										'TennisComplex'      => __( 'Tennis complex', 'yoast-local-seo' ),
									),
								),
								'Store'                       => array(
									'label'    => __( 'Store', 'yoast-local-seo' ),
									'children' => array(
										'AutoPartsStore'       => __( 'Auto parts store', 'yoast-local-seo' ),
										'BikeStore'            => __( 'Bike store', 'yoast-local-seo' ),
										'BookStore'            => __( 'Book store', 'yoast-local-seo' ),
										'ClothingStore'        => __( 'Clothing store', 'yoast-local-seo' ),
										'ComputerStore'        => __( 'Computer store', 'yoast-local-seo' ),
										'ConvenienceStore'     => __( 'Convenience store', 'yoast-local-seo' ),
										'DepartmentStore'      => __( 'Department store', 'yoast-local-seo' ),
										'ElectronicsStore'     => __( 'Electronics store', 'yoast-local-seo' ),
										'Florist'              => __( 'Florist', 'yoast-local-seo' ),
										'FurnitureStore'       => __( 'Furniture store', 'yoast-local-seo' ),
										'GardenStore'          => __( 'Garden store', 'yoast-local-seo' ),
										'GroceryStore'         => __( 'Grocery store', 'yoast-local-seo' ),
										'HardwareStore'        => __( 'Hardware store', 'yoast-local-seo' ),
										'HobbyShop'            => __( 'Hobby shop', 'yoast-local-seo' ),
										'HomeGoodsStore'       => __( 'Home goods store', 'yoast-local-seo' ),
										'JewelryStore'         => __( 'Jewelry store', 'yoast-local-seo' ),
										'LiquorStore'          => __( 'Liquor store', 'yoast-local-seo' ),
										'MensClothingStore'    => __( 'Mens clothing store', 'yoast-local-seo' ),
										'MobilePhoneStore'     => __( 'Mobile phone store', 'yoast-local-seo' ),
										'MovieRentalStore'     => __( 'Movie rental store', 'yoast-local-seo' ),
										'MusicStore'           => __( 'Music store', 'yoast-local-seo' ),
										'OfficeEquipmentStore' => __( 'Office equipment store', 'yoast-local-seo' ),
										'OutletStore'          => __( 'Outlet store', 'yoast-local-seo' ),
										'PawnShop'             => __( 'Pawn shop', 'yoast-local-seo' ),
										'PetStore'             => __( 'Pet store', 'yoast-local-seo' ),
										'ShoeStore'            => __( 'Shoe store', 'yoast-local-seo' ),
										'SportingGoodsStore'   => __( 'Sporting goods store', 'yoast-local-seo' ),
										'TireShop'             => __( 'Tire shop', 'yoast-local-seo' ),
										'ToyStore'             => __( 'Toy store', 'yoast-local-seo' ),
										'WholesaleStore'       => __( 'Wholesale store', 'yoast-local-seo' ),
									),
								),
								'TelevisionStation'           => __( 'Television station', 'yoast-local-seo' ),
								'TouristInformationCenter'    => __( 'Tourist information center', 'yoast-local-seo' ),
								'TravelAgency'                => __( 'Travel agency', 'yoast-local-seo' ),
							),
						),
						'MedicalOrganization'     => array(
							'label'    => __( 'Medical organization', 'yoast-local-seo' ),
							'children' => array(
								'Dentist'        => __( 'Dentist', 'yoast-local-seo' ),
								'DiagnosticLab'  => __( 'Diagnostic lab', 'yoast-local-seo' ),
								'Hospital'       => __( 'Hospital', 'yoast-local-seo' ),
								'MedicalClinic'  => __( 'Medical clinic', 'yoast-local-seo' ),
								'Pharmacy'       => __( 'Pharmacy', 'yoast-local-seo' ),
								'Physician'      => __( 'Physician', 'yoast-local-seo' ),
								'VeterinaryCare' => __( 'Veterinary care', 'yoast-local-seo' ),
							),
						),
						'NGO'                     => __( 'NGO', 'yoast-local-seo' ),
						'NewsMediaOrganization'   => __( 'News media organization', 'yoast-local-seo' ),
						'PerformingGroup'         => array(
							'label'    => __( 'Performing group', 'yoast-local-seo' ),
							'children' => array(
								'DanceGroup'   => __( 'Dance group', 'yoast-local-seo' ),
								'MusicGroup'   => __( 'Music group', 'yoast-local-seo' ),
								'TheaterGroup' => __( 'Theater group', 'yoast-local-seo' ),
							),
						),
						'Project'                 => array(
							'label'    => __( 'Project', 'yoast-local-seo' ),
							'children' => array(
								'FundingAgency'   => __( 'Funding agency', 'yoast-local-seo' ),
								'ResearchProject' => __( 'Research project', 'yoast-local-seo' ),
							),
						),
						'SportsOrganization'      => array(
							'label'    => __( 'Sports organization', 'yoast-local-seo' ),
							'children' => array(
								'SportsTeam' => __( 'Sports team', 'yoast-local-seo' ),
							),
						),
						'WorkersUnion'            => __( 'Workers union', 'yoast-local-seo' ),
					),
				),
			);

			$this->business_types = apply_filters( 'yoast-local-seo-business-types', $business_types );
		}

		/**
		 * Return a flat array of all Business Type options.
		 *
		 * @return array $business_types A flat array of all Business Types.
		 *
		 * @since 1.0
		 */
		public function get_business_types() {
			$this->flatten_business_types_array( $this->business_types );

			return $this->flattened_business_types;
		}

		/**
		 * Function to recursively go through the business types array and return a flat array to be used in
		 *
		 * @param     $array
		 * @param int $level
		 */
		private function flatten_business_types_array( $array, $level = 0 ) {
			if ( is_array( $array ) ) {
				foreach ( $array as $key => $value ) {
					//If $value is an array.
					if ( is_array( $value ) ) {
						//We need to loop through it.
						if ( isset( $value['children'] ) ) {
							$this->flattened_business_types[ $key ] = str_repeat( '&mdash; ', $level ) . $value['label'];
						}
						if ( isset( $value['children'] ) && ! empty( $value['children'] ) ) {
							$this->flatten_business_types_array( $value['children'], ( $level + 1 ) );
						}

						continue;
					}

					//It is not an array, so print it out.
					$this->flattened_business_types[ $key ] = str_repeat( '&mdash; ', $level ) . $value;
				}
			}
		}
	}
}
