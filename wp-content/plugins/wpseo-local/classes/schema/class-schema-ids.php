<?php
/**
 * @package WPSEO_Local\Frontend\Schema
 */

/**
 * Class WPSEO_Local_Schema_IDs.
 *
 * Defines all `@id` hashes we need throughout Local SEO's Schema.
 *
 * @property WPSEO_Schema_Context $context A value object with context variables.
 * @property array                $options Local SEO options.
 */
class WPSEO_Local_Schema_IDs {
	const PLACE_ID = '#local-place';
	const ADDRESS_ID = '#local-place-address';
	const ORGANIZATION_ID = '#local-organization';
	const ORGANIZATION_LOGO = '#local-org-logo';
	const LIST_ID = '#list';
}
