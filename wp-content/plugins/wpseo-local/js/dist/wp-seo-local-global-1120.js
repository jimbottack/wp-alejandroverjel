(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

jQuery(document).ready(function ($) {
    $('#use_multiple_locations').click(function () {
        if ($(this).is(':checked')) {
            $('#use_multiple_locations').attr('disabled', true);
            $('#single-location-settings').slideUp(function () {
                $('#multiple-locations-settings').slideDown();
                $('#sl-settings').slideDown();
                $('#opening-hours-hours').slideUp(function () {
                    $('#use_multiple_locations').removeAttr('disabled');
                });
            });
            $('.open_247_wrapper').slideUp();
            $('.default-setting').show();
        } else {
            $('#use_multiple_locations').attr('disabled', true);
            $('#multiple-locations-settings').slideUp(function () {
                $('#single-location-settings').slideDown();
                if (!$('#hide_opening_hours').is(':checked')) {
                    $('#opening-hours-hours').slideDown();
                }
                $('#sl-settings').slideUp();
                $('#use_multiple_locations').removeAttr('disabled');
            });
            $('.open_247_wrapper').slideDown();
            $('.default-setting').hide();
        }
    });

    $('#hide_opening_hours').click(function () {
        if ($(this).is(':checked')) {
            $('#opening-hours-hours, #opening-hours-settings').slideUp();
        } else {
            $('#opening-hours-settings').slideDown();
            if (!$('#use_multiple_locations').is(':checked')) {
                $('#opening-hours-hours').slideDown();
            }
        }
    });
    $('#multiple_opening_hours, #wpseo_multiple_opening_hours').click(function () {
        if ($(this).is(':checked')) {
            $('.opening-hours .opening-hours-second').slideDown();
        } else {
            $('.opening-hours .opening-hours-second').slideUp();
        }
    });
    $('#opening_hours_24h').click(function () {
        $('#opening-hours-container select').each(function () {
            $(this).find('option').each(function () {
                if ($('#opening_hours_24h').is(':checked')) {
                    // Use 24 hour
                    if ($(this).val() != 'closed') {
                        $(this).text($(this).val());
                    }
                } else {
                    // Use 12 hour
                    if ($(this).val() != 'closed') {
                        // Split the string between hours and minutes
                        var time = $(this).val().split(':');

                        // use parseInt to remove leading zeroes.
                        var hour = parseInt(time[0]);
                        var minutes = time[1];
                        var suffix = 'AM';
                        // if the hours number is greater than 12, subtract 12.
                        if (hour >= 12) {
                            if (hour > 12) {
                                hour = hour - 12;
                            }
                            suffix = 'PM';
                        }
                        if (hour == 0) {
                            hour = 12;
                        }

                        $(this).text(hour + ':' + minutes + ' ' + suffix);
                    }
                }
            });
        });
    });

    // The 24h format on single location page (if multiple locations is set)
    $('#wpseo_format_24h, #wpseo_format_12h').click(function () {
        $('#hide-opening-hours select').each(function () {
            $(this).find('option').each(function () {
                if ($('#wpseo_format_24h').length > 0 && $('#wpseo_format_24h').is(':checked') || $('#wpseo_format_12h').length > 0 && !$('#wpseo_format_12h').is(':checked')) {
                    // Use 24 hour
                    if ($(this).val() != 'closed') {
                        $(this).text($(this).val());
                    }
                } else {
                    // Use 12 hour
                    if ($(this).val() != 'closed') {
                        // Split the string between hours and minutes
                        var time = $(this).val().split(':');

                        // use parseInt to remove leading zeroes.
                        var hour = parseInt(time[0]);
                        var minutes = time[1];
                        var suffix = 'AM';
                        // if the hours number is greater than 12, subtract 12.
                        if (hour >= 12) {
                            if (hour > 12) {
                                hour = hour - 12;
                            }
                            suffix = 'PM';
                        }
                        if (hour == 0) {
                            hour = 12;
                        }

                        $(this).text(hour + ':' + minutes + ' ' + suffix);
                    }
                }
            });
        });
    });

    // General Settings: Enable/disable Open 24/7 on click
    $('#open_247').on('click', function () {
        if (!$('#use_multiple_locations').is(":checked")) {
            maybeCloseOpeningHours(this);
            $('.open_247_wrapper').show();
        }
    });

    // Single Location: Enable/disable Open 24/7 on click
    $('#wpseo_open_247').on('click', function () {
        maybeCloseOpeningHours(this);
    });

    // Disable hours 24/7 on click
    $('.wpseo_open_24h input').on('click', function (e) {
        if ($(this).is(":checked")) {
            $('select', $('.openinghours-wrapper', $(this).closest('.opening-hours'))).attr('disabled', true);
        } else {
            $('select', $('.openinghours-wrapper', $(this).closest('.opening-hours'))).attr('disabled', false);
        }
    });

    function maybeCloseOpeningHours(elem) {
        if ($(elem).is(':checked')) {
            $('#opening-hours-rows, .opening-hours-wrap').slideUp();
        } else {
            $('#opening-hours-rows, .opening-hours-wrap').slideDown();
        }
    }

    $('.widget-content').on('click', '#wpseo-checkbox-multiple-locations-wrapper input[type=checkbox]', function () {
        wpseo_show_all_locations_selectbox($(this));
    });

    // Show locations metabox before WP SEO metabox
    if ($('#wpseo_locations').length > 0 && $('#wpseo_meta').length > 0) {
        $('#wpseo_locations').insertBefore($('#wpseo_meta'));
    }

    $('.openinghours_from').change(function () {
        var to_id = $(this).attr('id').replace('_from', '_to_wrapper');
        var second_id = $(this).attr('id').replace('_from', '_second');

        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
            $('#' + second_id).css('display', 'none');
        } else {
            $('#' + to_id).css('display', 'inline');
            $('#' + second_id).css('display', 'block');
        }
    }).change();
    $('.openinghours_from_second').change(function () {
        var to_id = $(this).attr('id').replace('_from', '_to_wrapper');

        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
        } else {
            $('#' + to_id).css('display', 'inline');
        }
    }).change();
    $('.openinghours_to').change(function () {
        var from_id = $(this).attr('id').replace('_to', '_from');
        var to_id = $(this).attr('id').replace('_to', '_to_wrapper');
        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
            $('#' + from_id).val('closed');
        }
    });
    $('.openinghours_to_second').change(function () {
        var from_id = $(this).attr('id').replace('_to', '_from');
        var to_id = $(this).attr('id').replace('_to', '_to_wrapper');
        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
            $('#' + from_id).val('closed');
        }
    });

    if ($('.set_custom_images').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $('.wrap').on('click', '.set_custom_images', function (e) {
                e.preventDefault();
                var button = $(this);
                var id = button.attr('data-id');
                wp.media.editor.send.attachment = function (props, attachment) {
                    if (attachment.hasOwnProperty('sizes')) {
                        var url = attachment.sizes[props.size].url;
                    } else {
                        var url = attachment.url;
                    }

                    $('#' + id + '_image_container').attr('src', url);
                    $('.wpseo-local-' + id + '-wrapper .wpseo-local-hide-button').show();
                    $('#hidden_' + id).attr('value', attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }

    $('.remove_custom_image').on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');
        $('#' + id).attr('src', '').hide();
        $('#hidden_' + id).attr('value', '');
        $('.wpseo-local-' + id + '-wrapper .wpseo-local-hide-button').hide();
    });

    // Copy location data
    $('#wpseo_copy_from_location').change(function () {
        var location_id = $(this).val();

        if (location_id == '') return;

        $.post(wpseo_local_data.ajaxurl, {
            location_id: location_id,
            security: wpseo_local_data.sec_nonce,
            action: 'wpseo_copy_location'
        }, function (result) {
            if (result.charAt(result.length - 1) == 0) {
                result = result.slice(0, -1);
            } else if (result.substring(result.length - 2) == "-1") {
                result = result.slice(0, -2);
            }

            var data = $.parseJSON(result);
            if (data.success == 'true' || data.success == true) {

                for (var i in data.location) {
                    var value = data.location[i];

                    if (value != null && value != '' && typeof value != 'undefined') {
                        if (i == 'is_postal_address' || i == 'multiple_opening_hours') {
                            if (value == '1') {
                                $('#wpseo_' + i).attr('checked', 'checked');
                                $('.opening-hours .opening-hour-second').slideDown();
                            }
                        } else if (i.indexOf('opening_hours') > -1) {
                            $('#' + i).val(value);
                        } else {
                            $('#wpseo_' + i).val(value);
                        }
                    }
                }
            }
        });
    });
});

window.wpseo_show_all_locations_selectbox = function (obj) {
    $ = jQuery;

    $obj = $(obj);
    var parent = $obj.parents('.widget-inside');
    var $locationsWrapper = $('#wpseo-locations-wrapper', parent);

    if ($obj.is(':checked')) {
        $locationsWrapper.slideUp();
    } else {
        $locationsWrapper.slideDown();
    }
};

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqcy9zcmMvd3Atc2VvLWxvY2FsLWdsb2JhbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsT0FBTyxRQUFQLEVBQWlCLEtBQWpCLENBQXVCLFVBQVUsQ0FBVixFQUFhO0FBQ2hDLE1BQUUseUJBQUYsRUFBNkIsS0FBN0IsQ0FBbUMsWUFBWTtBQUMzQyxZQUFJLEVBQUUsSUFBRixFQUFRLEVBQVIsQ0FBVyxVQUFYLENBQUosRUFBNEI7QUFDeEIsY0FBRSx5QkFBRixFQUE2QixJQUE3QixDQUFrQyxVQUFsQyxFQUE4QyxJQUE5QztBQUNBLGNBQUUsMkJBQUYsRUFBK0IsT0FBL0IsQ0FBdUMsWUFBWTtBQUMvQyxrQkFBRSw4QkFBRixFQUFrQyxTQUFsQztBQUNBLGtCQUFFLGNBQUYsRUFBa0IsU0FBbEI7QUFDQSxrQkFBRSxzQkFBRixFQUEwQixPQUExQixDQUFrQyxZQUFZO0FBQzFDLHNCQUFFLHlCQUFGLEVBQTZCLFVBQTdCLENBQXdDLFVBQXhDO0FBQ0gsaUJBRkQ7QUFHSCxhQU5EO0FBT0EsY0FBRSxtQkFBRixFQUF1QixPQUF2QjtBQUNBLGNBQUUsa0JBQUYsRUFBc0IsSUFBdEI7QUFDSCxTQVhELE1BWUs7QUFDRCxjQUFFLHlCQUFGLEVBQTZCLElBQTdCLENBQWtDLFVBQWxDLEVBQThDLElBQTlDO0FBQ0EsY0FBRSw4QkFBRixFQUFrQyxPQUFsQyxDQUEwQyxZQUFZO0FBQ2xELGtCQUFFLDJCQUFGLEVBQStCLFNBQS9CO0FBQ0Esb0JBQUksQ0FBQyxFQUFFLHFCQUFGLEVBQXlCLEVBQXpCLENBQTRCLFVBQTVCLENBQUwsRUFBOEM7QUFDMUMsc0JBQUUsc0JBQUYsRUFBMEIsU0FBMUI7QUFDSDtBQUNELGtCQUFFLGNBQUYsRUFBa0IsT0FBbEI7QUFDQSxrQkFBRSx5QkFBRixFQUE2QixVQUE3QixDQUF3QyxVQUF4QztBQUNILGFBUEQ7QUFRQSxjQUFFLG1CQUFGLEVBQXVCLFNBQXZCO0FBQ0EsY0FBRSxrQkFBRixFQUFzQixJQUF0QjtBQUNIO0FBQ0osS0ExQkQ7O0FBNEJBLE1BQUUscUJBQUYsRUFBeUIsS0FBekIsQ0FBK0IsWUFBWTtBQUN2QyxZQUFJLEVBQUUsSUFBRixFQUFRLEVBQVIsQ0FBVyxVQUFYLENBQUosRUFBNEI7QUFDeEIsY0FBRSwrQ0FBRixFQUFtRCxPQUFuRDtBQUNILFNBRkQsTUFHSztBQUNELGNBQUUseUJBQUYsRUFBNkIsU0FBN0I7QUFDQSxnQkFBSSxDQUFDLEVBQUUseUJBQUYsRUFBNkIsRUFBN0IsQ0FBZ0MsVUFBaEMsQ0FBTCxFQUFrRDtBQUM5QyxrQkFBRSxzQkFBRixFQUEwQixTQUExQjtBQUNIO0FBQ0o7QUFDSixLQVZEO0FBV0EsTUFBRSx3REFBRixFQUE0RCxLQUE1RCxDQUFrRSxZQUFZO0FBQzFFLFlBQUksRUFBRSxJQUFGLEVBQVEsRUFBUixDQUFXLFVBQVgsQ0FBSixFQUE0QjtBQUN4QixjQUFFLHNDQUFGLEVBQTBDLFNBQTFDO0FBQ0gsU0FGRCxNQUdLO0FBQ0QsY0FBRSxzQ0FBRixFQUEwQyxPQUExQztBQUNIO0FBQ0osS0FQRDtBQVFBLE1BQUUsb0JBQUYsRUFBd0IsS0FBeEIsQ0FBOEIsWUFBWTtBQUN0QyxVQUFFLGlDQUFGLEVBQXFDLElBQXJDLENBQTBDLFlBQVk7QUFDbEQsY0FBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFFBQWIsRUFBdUIsSUFBdkIsQ0FBNEIsWUFBWTtBQUNwQyxvQkFBSSxFQUFFLG9CQUFGLEVBQXdCLEVBQXhCLENBQTJCLFVBQTNCLENBQUosRUFBNEM7QUFDeEM7QUFDQSx3QkFBSSxFQUFFLElBQUYsRUFBUSxHQUFSLE1BQWlCLFFBQXJCLEVBQStCO0FBQzNCLDBCQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsRUFBRSxJQUFGLEVBQVEsR0FBUixFQUFiO0FBQ0g7QUFDSixpQkFMRCxNQUtPO0FBQ0g7QUFDQSx3QkFBSSxFQUFFLElBQUYsRUFBUSxHQUFSLE1BQWlCLFFBQXJCLEVBQStCO0FBQzNCO0FBQ0EsNEJBQUksT0FBTyxFQUFFLElBQUYsRUFBUSxHQUFSLEdBQWMsS0FBZCxDQUFvQixHQUFwQixDQUFYOztBQUVBO0FBQ0EsNEJBQUksT0FBTyxTQUFTLEtBQUssQ0FBTCxDQUFULENBQVg7QUFDQSw0QkFBSSxVQUFVLEtBQUssQ0FBTCxDQUFkO0FBQ0EsNEJBQUksU0FBUyxJQUFiO0FBQ0E7QUFDQSw0QkFBSSxRQUFRLEVBQVosRUFBZ0I7QUFDWixnQ0FBSSxPQUFPLEVBQVgsRUFBZTtBQUNYLHVDQUFPLE9BQU8sRUFBZDtBQUNIO0FBQ0QscUNBQVMsSUFBVDtBQUNIO0FBQ0QsNEJBQUksUUFBUSxDQUFaLEVBQWU7QUFDWCxtQ0FBTyxFQUFQO0FBQ0g7O0FBRUQsMEJBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxPQUFPLEdBQVAsR0FBYSxPQUFiLEdBQXVCLEdBQXZCLEdBQTZCLE1BQTFDO0FBQ0g7QUFDSjtBQUNKLGFBOUJEO0FBK0JILFNBaENEO0FBaUNILEtBbENEOztBQW9DQTtBQUNBLE1BQUUsc0NBQUYsRUFBMEMsS0FBMUMsQ0FBZ0QsWUFBWTtBQUN4RCxVQUFFLDRCQUFGLEVBQWdDLElBQWhDLENBQXFDLFlBQVk7QUFDN0MsY0FBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFFBQWIsRUFBdUIsSUFBdkIsQ0FBNEIsWUFBWTtBQUNwQyxvQkFBTyxFQUFFLG1CQUFGLEVBQXVCLE1BQXZCLEdBQWdDLENBQWhDLElBQXFDLEVBQUUsbUJBQUYsRUFBdUIsRUFBdkIsQ0FBMEIsVUFBMUIsQ0FBdkMsSUFBc0YsRUFBRSxtQkFBRixFQUF1QixNQUF2QixHQUFnQyxDQUFoQyxJQUFxQyxDQUFFLEVBQUUsbUJBQUYsRUFBdUIsRUFBdkIsQ0FBMEIsVUFBMUIsQ0FBbEksRUFBOEs7QUFDMUs7QUFDQSx3QkFBSSxFQUFFLElBQUYsRUFBUSxHQUFSLE1BQWlCLFFBQXJCLEVBQStCO0FBQzNCLDBCQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsRUFBRSxJQUFGLEVBQVEsR0FBUixFQUFiO0FBQ0g7QUFDSixpQkFMRCxNQUtPO0FBQ0g7QUFDQSx3QkFBSSxFQUFFLElBQUYsRUFBUSxHQUFSLE1BQWlCLFFBQXJCLEVBQStCO0FBQzNCO0FBQ0EsNEJBQUksT0FBTyxFQUFFLElBQUYsRUFBUSxHQUFSLEdBQWMsS0FBZCxDQUFvQixHQUFwQixDQUFYOztBQUVBO0FBQ0EsNEJBQUksT0FBTyxTQUFTLEtBQUssQ0FBTCxDQUFULENBQVg7QUFDQSw0QkFBSSxVQUFVLEtBQUssQ0FBTCxDQUFkO0FBQ0EsNEJBQUksU0FBUyxJQUFiO0FBQ0E7QUFDQSw0QkFBSSxRQUFRLEVBQVosRUFBZ0I7QUFDWixnQ0FBSSxPQUFPLEVBQVgsRUFBZTtBQUNYLHVDQUFPLE9BQU8sRUFBZDtBQUNIO0FBQ0QscUNBQVMsSUFBVDtBQUNIO0FBQ0QsNEJBQUksUUFBUSxDQUFaLEVBQWU7QUFDWCxtQ0FBTyxFQUFQO0FBQ0g7O0FBRUQsMEJBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxPQUFPLEdBQVAsR0FBYSxPQUFiLEdBQXVCLEdBQXZCLEdBQTZCLE1BQTFDO0FBQ0g7QUFDSjtBQUNKLGFBOUJEO0FBK0JILFNBaENEO0FBaUNILEtBbENEOztBQW9DQTtBQUNBLE1BQUUsV0FBRixFQUFlLEVBQWYsQ0FBa0IsT0FBbEIsRUFBMkIsWUFBWTtBQUNuQyxZQUFJLENBQUMsRUFBRSx5QkFBRixFQUE2QixFQUE3QixDQUFnQyxVQUFoQyxDQUFMLEVBQWtEO0FBQzlDLG1DQUF1QixJQUF2QjtBQUNBLGNBQUUsbUJBQUYsRUFBdUIsSUFBdkI7QUFDSDtBQUNKLEtBTEQ7O0FBT0E7QUFDQSxNQUFFLGlCQUFGLEVBQXFCLEVBQXJCLENBQXdCLE9BQXhCLEVBQWlDLFlBQVk7QUFDekMsK0JBQXVCLElBQXZCO0FBQ0gsS0FGRDs7QUFJQTtBQUNBLE1BQUUsdUJBQUYsRUFBMkIsRUFBM0IsQ0FBOEIsT0FBOUIsRUFBdUMsVUFBVSxDQUFWLEVBQWE7QUFDaEQsWUFBSSxFQUFFLElBQUYsRUFBUSxFQUFSLENBQVcsVUFBWCxDQUFKLEVBQTRCO0FBQ3hCLGNBQUUsUUFBRixFQUFZLEVBQUUsdUJBQUYsRUFBMkIsRUFBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixnQkFBaEIsQ0FBM0IsQ0FBWixFQUEyRSxJQUEzRSxDQUFnRixVQUFoRixFQUE0RixJQUE1RjtBQUNILFNBRkQsTUFFTztBQUNILGNBQUUsUUFBRixFQUFZLEVBQUUsdUJBQUYsRUFBMkIsRUFBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixnQkFBaEIsQ0FBM0IsQ0FBWixFQUEyRSxJQUEzRSxDQUFnRixVQUFoRixFQUE0RixLQUE1RjtBQUNIO0FBQ0osS0FORDs7QUFRQSxhQUFTLHNCQUFULENBQWdDLElBQWhDLEVBQXNDO0FBQ2xDLFlBQUksRUFBRSxJQUFGLEVBQVEsRUFBUixDQUFXLFVBQVgsQ0FBSixFQUE0QjtBQUN4QixjQUFFLDBDQUFGLEVBQThDLE9BQTlDO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsY0FBRSwwQ0FBRixFQUE4QyxTQUE5QztBQUNIO0FBQ0o7O0FBRUQsTUFBRSxpQkFBRixFQUFxQixFQUFyQixDQUF3QixPQUF4QixFQUFpQyxpRUFBakMsRUFBb0csWUFBWTtBQUM1RywyQ0FBbUMsRUFBRSxJQUFGLENBQW5DO0FBQ0gsS0FGRDs7QUFJQTtBQUNBLFFBQUksRUFBRSxrQkFBRixFQUFzQixNQUF0QixHQUErQixDQUEvQixJQUFvQyxFQUFFLGFBQUYsRUFBaUIsTUFBakIsR0FBMEIsQ0FBbEUsRUFBcUU7QUFDakUsVUFBRSxrQkFBRixFQUFzQixZQUF0QixDQUFtQyxFQUFFLGFBQUYsQ0FBbkM7QUFDSDs7QUFFRCxNQUFFLG9CQUFGLEVBQXdCLE1BQXhCLENBQStCLFlBQVk7QUFDdkMsWUFBSSxRQUFRLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CLE9BQW5CLENBQTJCLE9BQTNCLEVBQW9DLGFBQXBDLENBQVo7QUFDQSxZQUFJLFlBQVksRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLElBQWIsRUFBbUIsT0FBbkIsQ0FBMkIsT0FBM0IsRUFBb0MsU0FBcEMsQ0FBaEI7O0FBRUEsWUFBSSxFQUFFLElBQUYsRUFBUSxHQUFSLE1BQWlCLFFBQXJCLEVBQStCO0FBQzNCLGNBQUUsTUFBTSxLQUFSLEVBQWUsR0FBZixDQUFtQixTQUFuQixFQUE4QixNQUE5QjtBQUNBLGNBQUUsTUFBTSxTQUFSLEVBQW1CLEdBQW5CLENBQXVCLFNBQXZCLEVBQWtDLE1BQWxDO0FBQ0gsU0FIRCxNQUlLO0FBQ0QsY0FBRSxNQUFNLEtBQVIsRUFBZSxHQUFmLENBQW1CLFNBQW5CLEVBQThCLFFBQTlCO0FBQ0EsY0FBRSxNQUFNLFNBQVIsRUFBbUIsR0FBbkIsQ0FBdUIsU0FBdkIsRUFBa0MsT0FBbEM7QUFDSDtBQUNKLEtBWkQsRUFZRyxNQVpIO0FBYUEsTUFBRSwyQkFBRixFQUErQixNQUEvQixDQUFzQyxZQUFZO0FBQzlDLFlBQUksUUFBUSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsSUFBYixFQUFtQixPQUFuQixDQUEyQixPQUEzQixFQUFvQyxhQUFwQyxDQUFaOztBQUVBLFlBQUksRUFBRSxJQUFGLEVBQVEsR0FBUixNQUFpQixRQUFyQixFQUErQjtBQUMzQixjQUFFLE1BQU0sS0FBUixFQUFlLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsTUFBOUI7QUFDSCxTQUZELE1BR0s7QUFDRCxjQUFFLE1BQU0sS0FBUixFQUFlLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsUUFBOUI7QUFDSDtBQUNKLEtBVEQsRUFTRyxNQVRIO0FBVUEsTUFBRSxrQkFBRixFQUFzQixNQUF0QixDQUE2QixZQUFZO0FBQ3JDLFlBQUksVUFBVSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsSUFBYixFQUFtQixPQUFuQixDQUEyQixLQUEzQixFQUFrQyxPQUFsQyxDQUFkO0FBQ0EsWUFBSSxRQUFRLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CLE9BQW5CLENBQTJCLEtBQTNCLEVBQWtDLGFBQWxDLENBQVo7QUFDQSxZQUFJLEVBQUUsSUFBRixFQUFRLEdBQVIsTUFBaUIsUUFBckIsRUFBK0I7QUFDM0IsY0FBRSxNQUFNLEtBQVIsRUFBZSxHQUFmLENBQW1CLFNBQW5CLEVBQThCLE1BQTlCO0FBQ0EsY0FBRSxNQUFNLE9BQVIsRUFBaUIsR0FBakIsQ0FBcUIsUUFBckI7QUFDSDtBQUNKLEtBUEQ7QUFRQSxNQUFFLHlCQUFGLEVBQTZCLE1BQTdCLENBQW9DLFlBQVk7QUFDNUMsWUFBSSxVQUFVLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CLE9BQW5CLENBQTJCLEtBQTNCLEVBQWtDLE9BQWxDLENBQWQ7QUFDQSxZQUFJLFFBQVEsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLElBQWIsRUFBbUIsT0FBbkIsQ0FBMkIsS0FBM0IsRUFBa0MsYUFBbEMsQ0FBWjtBQUNBLFlBQUksRUFBRSxJQUFGLEVBQVEsR0FBUixNQUFpQixRQUFyQixFQUErQjtBQUMzQixjQUFFLE1BQU0sS0FBUixFQUFlLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsTUFBOUI7QUFDQSxjQUFFLE1BQU0sT0FBUixFQUFpQixHQUFqQixDQUFxQixRQUFyQjtBQUNIO0FBQ0osS0FQRDs7QUFTQSxRQUFJLEVBQUUsb0JBQUYsRUFBd0IsTUFBeEIsR0FBaUMsQ0FBckMsRUFBd0M7QUFDcEMsWUFBSSxPQUFPLEVBQVAsS0FBYyxXQUFkLElBQTZCLEdBQUcsS0FBaEMsSUFBeUMsR0FBRyxLQUFILENBQVMsTUFBdEQsRUFBOEQ7QUFDMUQsY0FBRSxPQUFGLEVBQVcsRUFBWCxDQUFjLE9BQWQsRUFBdUIsb0JBQXZCLEVBQTZDLFVBQVUsQ0FBVixFQUFhO0FBQ3RELGtCQUFFLGNBQUY7QUFDQSxvQkFBSSxTQUFTLEVBQUUsSUFBRixDQUFiO0FBQ0Esb0JBQUksS0FBSyxPQUFPLElBQVAsQ0FBWSxTQUFaLENBQVQ7QUFDQSxtQkFBRyxLQUFILENBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixVQUFyQixHQUFrQyxVQUFVLEtBQVYsRUFBaUIsVUFBakIsRUFBNkI7QUFDM0Qsd0JBQUksV0FBVyxjQUFYLENBQTBCLE9BQTFCLENBQUosRUFBd0M7QUFDcEMsNEJBQUksTUFBTSxXQUFXLEtBQVgsQ0FBaUIsTUFBTSxJQUF2QixFQUE2QixHQUF2QztBQUNILHFCQUZELE1BRU87QUFDSCw0QkFBSSxNQUFNLFdBQVcsR0FBckI7QUFDSDs7QUFFRCxzQkFBRSxNQUFNLEVBQU4sR0FBVyxrQkFBYixFQUFpQyxJQUFqQyxDQUFzQyxLQUF0QyxFQUE2QyxHQUE3QztBQUNBLHNCQUFFLGtCQUFrQixFQUFsQixHQUF1QixtQ0FBekIsRUFBOEQsSUFBOUQ7QUFDQSxzQkFBRSxhQUFhLEVBQWYsRUFBbUIsSUFBbkIsQ0FBd0IsT0FBeEIsRUFBaUMsV0FBVyxFQUE1QztBQUNILGlCQVZEO0FBV0EsbUJBQUcsS0FBSCxDQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsTUFBckI7QUFDQSx1QkFBTyxLQUFQO0FBQ0gsYUFqQkQ7QUFrQkg7QUFDSjs7QUFFRCxNQUFFLHNCQUFGLEVBQTBCLEVBQTFCLENBQTZCLE9BQTdCLEVBQXNDLFVBQVUsQ0FBVixFQUFhO0FBQy9DLFVBQUUsY0FBRjs7QUFFQSxZQUFJLEtBQUssRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFNBQWIsQ0FBVDtBQUNBLFVBQUUsTUFBTSxFQUFSLEVBQVksSUFBWixDQUFpQixLQUFqQixFQUF3QixFQUF4QixFQUE0QixJQUE1QjtBQUNBLFVBQUUsYUFBYSxFQUFmLEVBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLEVBQWpDO0FBQ0EsVUFBRSxrQkFBa0IsRUFBbEIsR0FBdUIsbUNBQXpCLEVBQThELElBQTlEO0FBQ0gsS0FQRDs7QUFTQTtBQUNBLE1BQUUsMkJBQUYsRUFBK0IsTUFBL0IsQ0FBc0MsWUFBWTtBQUM5QyxZQUFJLGNBQWMsRUFBRSxJQUFGLEVBQVEsR0FBUixFQUFsQjs7QUFFQSxZQUFJLGVBQWUsRUFBbkIsRUFDSTs7QUFFSixVQUFFLElBQUYsQ0FBTyxpQkFBaUIsT0FBeEIsRUFBaUM7QUFDN0IseUJBQWEsV0FEZ0I7QUFFN0Isc0JBQVUsaUJBQWlCLFNBRkU7QUFHN0Isb0JBQVE7QUFIcUIsU0FBakMsRUFJRyxVQUFVLE1BQVYsRUFBa0I7QUFDakIsZ0JBQUksT0FBTyxNQUFQLENBQWMsT0FBTyxNQUFQLEdBQWdCLENBQTlCLEtBQW9DLENBQXhDLEVBQTJDO0FBQ3ZDLHlCQUFTLE9BQU8sS0FBUCxDQUFhLENBQWIsRUFBZ0IsQ0FBQyxDQUFqQixDQUFUO0FBQ0gsYUFGRCxNQUdLLElBQUksT0FBTyxTQUFQLENBQWlCLE9BQU8sTUFBUCxHQUFnQixDQUFqQyxLQUF1QyxJQUEzQyxFQUFpRDtBQUNsRCx5QkFBUyxPQUFPLEtBQVAsQ0FBYSxDQUFiLEVBQWdCLENBQUMsQ0FBakIsQ0FBVDtBQUNIOztBQUVELGdCQUFJLE9BQU8sRUFBRSxTQUFGLENBQVksTUFBWixDQUFYO0FBQ0EsZ0JBQUksS0FBSyxPQUFMLElBQWdCLE1BQWhCLElBQTBCLEtBQUssT0FBTCxJQUFnQixJQUE5QyxFQUFvRDs7QUFFaEQscUJBQUssSUFBSSxDQUFULElBQWMsS0FBSyxRQUFuQixFQUE2QjtBQUN6Qix3QkFBSSxRQUFRLEtBQUssUUFBTCxDQUFjLENBQWQsQ0FBWjs7QUFFQSx3QkFBSSxTQUFTLElBQVQsSUFBaUIsU0FBUyxFQUExQixJQUFnQyxPQUFPLEtBQVAsSUFBZ0IsV0FBcEQsRUFBaUU7QUFDN0QsNEJBQUksS0FBSyxtQkFBTCxJQUE0QixLQUFLLHdCQUFyQyxFQUErRDtBQUMzRCxnQ0FBSSxTQUFTLEdBQWIsRUFBa0I7QUFDZCxrQ0FBRSxZQUFZLENBQWQsRUFBaUIsSUFBakIsQ0FBc0IsU0FBdEIsRUFBaUMsU0FBakM7QUFDQSxrQ0FBRSxxQ0FBRixFQUF5QyxTQUF6QztBQUNIO0FBQ0oseUJBTEQsTUFNSyxJQUFJLEVBQUUsT0FBRixDQUFVLGVBQVYsSUFBNkIsQ0FBQyxDQUFsQyxFQUFxQztBQUN0Qyw4QkFBRSxNQUFNLENBQVIsRUFBVyxHQUFYLENBQWUsS0FBZjtBQUNILHlCQUZJLE1BR0E7QUFDRCw4QkFBRSxZQUFZLENBQWQsRUFBaUIsR0FBakIsQ0FBcUIsS0FBckI7QUFDSDtBQUNKO0FBQ0o7QUFDSjtBQUNKLFNBbENEO0FBbUNILEtBekNEO0FBMENILENBblJEOztBQXFSQSxPQUFPLGtDQUFQLEdBQTRDLFVBQVUsR0FBVixFQUFlO0FBQ3ZELFFBQUksTUFBSjs7QUFFQSxXQUFPLEVBQUUsR0FBRixDQUFQO0FBQ0EsUUFBSSxTQUFTLEtBQUssT0FBTCxDQUFhLGdCQUFiLENBQWI7QUFDQSxRQUFJLG9CQUFvQixFQUFFLDBCQUFGLEVBQThCLE1BQTlCLENBQXhCOztBQUVBLFFBQUksS0FBSyxFQUFMLENBQVEsVUFBUixDQUFKLEVBQXlCO0FBQ3JCLDBCQUFrQixPQUFsQjtBQUNILEtBRkQsTUFHSztBQUNELDBCQUFrQixTQUFsQjtBQUNIO0FBQ0osQ0FiRCIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsImpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCQpIHtcbiAgICAkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCQodGhpcykuaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICAgICAgICQoJyN1c2VfbXVsdGlwbGVfbG9jYXRpb25zJykuYXR0cignZGlzYWJsZWQnLCB0cnVlKTtcbiAgICAgICAgICAgICQoJyNzaW5nbGUtbG9jYXRpb24tc2V0dGluZ3MnKS5zbGlkZVVwKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkKCcjbXVsdGlwbGUtbG9jYXRpb25zLXNldHRpbmdzJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICAgICAgJCgnI3NsLXNldHRpbmdzJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICAgICAgJCgnI29wZW5pbmctaG91cnMtaG91cnMnKS5zbGlkZVVwKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnI3VzZV9tdWx0aXBsZV9sb2NhdGlvbnMnKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkKCcub3Blbl8yNDdfd3JhcHBlcicpLnNsaWRlVXAoKTtcbiAgICAgICAgICAgICQoJy5kZWZhdWx0LXNldHRpbmcnKS5zaG93KCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLmF0dHIoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICAkKCcjbXVsdGlwbGUtbG9jYXRpb25zLXNldHRpbmdzJykuc2xpZGVVcChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJCgnI3NpbmdsZS1sb2NhdGlvbi1zZXR0aW5ncycpLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgICAgIGlmICghJCgnI2hpZGVfb3BlbmluZ19ob3VycycpLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLWhvdXJzJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICQoJyNzbC1zZXR0aW5ncycpLnNsaWRlVXAoKTtcbiAgICAgICAgICAgICAgICAkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoJy5vcGVuXzI0N193cmFwcGVyJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICAkKCcuZGVmYXVsdC1zZXR0aW5nJykuaGlkZSgpO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAkKCcjaGlkZV9vcGVuaW5nX2hvdXJzJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoJCh0aGlzKS5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgICAgICAgJCgnI29wZW5pbmctaG91cnMtaG91cnMsICNvcGVuaW5nLWhvdXJzLXNldHRpbmdzJykuc2xpZGVVcCgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJCgnI29wZW5pbmctaG91cnMtc2V0dGluZ3MnKS5zbGlkZURvd24oKTtcbiAgICAgICAgICAgIGlmICghJCgnI3VzZV9tdWx0aXBsZV9sb2NhdGlvbnMnKS5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgICAgICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLWhvdXJzJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAkKCcjbXVsdGlwbGVfb3BlbmluZ19ob3VycywgI3dwc2VvX211bHRpcGxlX29wZW5pbmdfaG91cnMnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICgkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAkKCcub3BlbmluZy1ob3VycyAub3BlbmluZy1ob3Vycy1zZWNvbmQnKS5zbGlkZURvd24oKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICQoJy5vcGVuaW5nLWhvdXJzIC5vcGVuaW5nLWhvdXJzLXNlY29uZCcpLnNsaWRlVXAoKTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgICQoJyNvcGVuaW5nX2hvdXJzXzI0aCcpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnI29wZW5pbmctaG91cnMtY29udGFpbmVyIHNlbGVjdCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCh0aGlzKS5maW5kKCdvcHRpb24nKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBpZiAoJCgnI29wZW5pbmdfaG91cnNfMjRoJykuaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gVXNlIDI0IGhvdXJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCQodGhpcykudmFsKCkgIT0gJ2Nsb3NlZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykudGV4dCgkKHRoaXMpLnZhbCgpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIFVzZSAxMiBob3VyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLnZhbCgpICE9ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBTcGxpdCB0aGUgc3RyaW5nIGJldHdlZW4gaG91cnMgYW5kIG1pbnV0ZXNcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0aW1lID0gJCh0aGlzKS52YWwoKS5zcGxpdCgnOicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c2UgcGFyc2VJbnQgdG8gcmVtb3ZlIGxlYWRpbmcgemVyb2VzLlxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGhvdXIgPSBwYXJzZUludCh0aW1lWzBdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBtaW51dGVzID0gdGltZVsxXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzdWZmaXggPSAnQU0nO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIGhvdXJzIG51bWJlciBpcyBncmVhdGVyIHRoYW4gMTIsIHN1YnRyYWN0IDEyLlxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhvdXIgPj0gMTIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaG91ciA+IDEyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdXIgPSBob3VyIC0gMTI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1ZmZpeCA9ICdQTSc7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaG91ciA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaG91ciA9IDEyO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnRleHQoaG91ciArICc6JyArIG1pbnV0ZXMgKyAnICcgKyBzdWZmaXgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gVGhlIDI0aCBmb3JtYXQgb24gc2luZ2xlIGxvY2F0aW9uIHBhZ2UgKGlmIG11bHRpcGxlIGxvY2F0aW9ucyBpcyBzZXQpXG4gICAgJCgnI3dwc2VvX2Zvcm1hdF8yNGgsICN3cHNlb19mb3JtYXRfMTJoJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCcjaGlkZS1vcGVuaW5nLWhvdXJzIHNlbGVjdCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCh0aGlzKS5maW5kKCdvcHRpb24nKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBpZiAoICggJCgnI3dwc2VvX2Zvcm1hdF8yNGgnKS5sZW5ndGggPiAwICYmICQoJyN3cHNlb19mb3JtYXRfMjRoJykuaXMoJzpjaGVja2VkJykgKSB8fCAoICggJCgnI3dwc2VvX2Zvcm1hdF8xMmgnKS5sZW5ndGggPiAwICYmICEgJCgnI3dwc2VvX2Zvcm1hdF8xMmgnKS5pcygnOmNoZWNrZWQnKSApICkgKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIFVzZSAyNCBob3VyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLnZhbCgpICE9ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnRleHQoJCh0aGlzKS52YWwoKSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAvLyBVc2UgMTIgaG91clxuICAgICAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS52YWwoKSAhPSAnY2xvc2VkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gU3BsaXQgdGhlIHN0cmluZyBiZXR3ZWVuIGhvdXJzIGFuZCBtaW51dGVzXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGltZSA9ICQodGhpcykudmFsKCkuc3BsaXQoJzonKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXNlIHBhcnNlSW50IHRvIHJlbW92ZSBsZWFkaW5nIHplcm9lcy5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBob3VyID0gcGFyc2VJbnQodGltZVswXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbWludXRlcyA9IHRpbWVbMV07XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3VmZml4ID0gJ0FNJztcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBob3VycyBudW1iZXIgaXMgZ3JlYXRlciB0aGFuIDEyLCBzdWJ0cmFjdCAxMi5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChob3VyID49IDEyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhvdXIgPiAxMikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3VyID0gaG91ciAtIDEyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWZmaXggPSAnUE0nO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhvdXIgPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdXIgPSAxMjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS50ZXh0KGhvdXIgKyAnOicgKyBtaW51dGVzICsgJyAnICsgc3VmZml4KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIEdlbmVyYWwgU2V0dGluZ3M6IEVuYWJsZS9kaXNhYmxlIE9wZW4gMjQvNyBvbiBjbGlja1xuICAgICQoJyNvcGVuXzI0NycpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCEkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLmlzKFwiOmNoZWNrZWRcIikpIHtcbiAgICAgICAgICAgIG1heWJlQ2xvc2VPcGVuaW5nSG91cnModGhpcyk7XG4gICAgICAgICAgICAkKCcub3Blbl8yNDdfd3JhcHBlcicpLnNob3coKVxuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAvLyBTaW5nbGUgTG9jYXRpb246IEVuYWJsZS9kaXNhYmxlIE9wZW4gMjQvNyBvbiBjbGlja1xuICAgICQoJyN3cHNlb19vcGVuXzI0NycpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbWF5YmVDbG9zZU9wZW5pbmdIb3Vycyh0aGlzKTtcbiAgICB9KTtcblxuICAgIC8vIERpc2FibGUgaG91cnMgMjQvNyBvbiBjbGlja1xuICAgICQoJy53cHNlb19vcGVuXzI0aCBpbnB1dCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGlmICgkKHRoaXMpLmlzKFwiOmNoZWNrZWRcIikpIHtcbiAgICAgICAgICAgICQoJ3NlbGVjdCcsICQoJy5vcGVuaW5naG91cnMtd3JhcHBlcicsICQodGhpcykuY2xvc2VzdCgnLm9wZW5pbmctaG91cnMnKSkpLmF0dHIoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKCdzZWxlY3QnLCAkKCcub3BlbmluZ2hvdXJzLXdyYXBwZXInLCAkKHRoaXMpLmNsb3Nlc3QoJy5vcGVuaW5nLWhvdXJzJykpKS5hdHRyKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgZnVuY3Rpb24gbWF5YmVDbG9zZU9wZW5pbmdIb3VycyhlbGVtKSB7XG4gICAgICAgIGlmICgkKGVsZW0pLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAkKCcjb3BlbmluZy1ob3Vycy1yb3dzLCAub3BlbmluZy1ob3Vycy13cmFwJykuc2xpZGVVcCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCgnI29wZW5pbmctaG91cnMtcm93cywgLm9wZW5pbmctaG91cnMtd3JhcCcpLnNsaWRlRG93bigpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgJCgnLndpZGdldC1jb250ZW50Jykub24oJ2NsaWNrJywgJyN3cHNlby1jaGVja2JveC1tdWx0aXBsZS1sb2NhdGlvbnMtd3JhcHBlciBpbnB1dFt0eXBlPWNoZWNrYm94XScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgd3BzZW9fc2hvd19hbGxfbG9jYXRpb25zX3NlbGVjdGJveCgkKHRoaXMpKTtcbiAgICB9KTtcblxuICAgIC8vIFNob3cgbG9jYXRpb25zIG1ldGFib3ggYmVmb3JlIFdQIFNFTyBtZXRhYm94XG4gICAgaWYgKCQoJyN3cHNlb19sb2NhdGlvbnMnKS5sZW5ndGggPiAwICYmICQoJyN3cHNlb19tZXRhJykubGVuZ3RoID4gMCkge1xuICAgICAgICAkKCcjd3BzZW9fbG9jYXRpb25zJykuaW5zZXJ0QmVmb3JlKCQoJyN3cHNlb19tZXRhJykpO1xuICAgIH1cblxuICAgICQoJy5vcGVuaW5naG91cnNfZnJvbScpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciB0b19pZCA9ICQodGhpcykuYXR0cignaWQnKS5yZXBsYWNlKCdfZnJvbScsICdfdG9fd3JhcHBlcicpO1xuICAgICAgICB2YXIgc2Vjb25kX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpLnJlcGxhY2UoJ19mcm9tJywgJ19zZWNvbmQnKTtcblxuICAgICAgICBpZiAoJCh0aGlzKS52YWwoKSA9PSAnY2xvc2VkJykge1xuICAgICAgICAgICAgJCgnIycgKyB0b19pZCkuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgICAgICAgICQoJyMnICsgc2Vjb25kX2lkKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJCgnIycgKyB0b19pZCkuY3NzKCdkaXNwbGF5JywgJ2lubGluZScpO1xuICAgICAgICAgICAgJCgnIycgKyBzZWNvbmRfaWQpLmNzcygnZGlzcGxheScsICdibG9jaycpO1xuICAgICAgICB9XG4gICAgfSkuY2hhbmdlKCk7XG4gICAgJCgnLm9wZW5pbmdob3Vyc19mcm9tX3NlY29uZCcpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciB0b19pZCA9ICQodGhpcykuYXR0cignaWQnKS5yZXBsYWNlKCdfZnJvbScsICdfdG9fd3JhcHBlcicpO1xuXG4gICAgICAgIGlmICgkKHRoaXMpLnZhbCgpID09ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAkKCcjJyArIHRvX2lkKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJCgnIycgKyB0b19pZCkuY3NzKCdkaXNwbGF5JywgJ2lubGluZScpO1xuICAgICAgICB9XG4gICAgfSkuY2hhbmdlKCk7XG4gICAgJCgnLm9wZW5pbmdob3Vyc190bycpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBmcm9tX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpLnJlcGxhY2UoJ190bycsICdfZnJvbScpO1xuICAgICAgICB2YXIgdG9faWQgPSAkKHRoaXMpLmF0dHIoJ2lkJykucmVwbGFjZSgnX3RvJywgJ190b193cmFwcGVyJyk7XG4gICAgICAgIGlmICgkKHRoaXMpLnZhbCgpID09ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAkKCcjJyArIHRvX2lkKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgJCgnIycgKyBmcm9tX2lkKS52YWwoJ2Nsb3NlZCcpO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgJCgnLm9wZW5pbmdob3Vyc190b19zZWNvbmQnKS5jaGFuZ2UoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZnJvbV9pZCA9ICQodGhpcykuYXR0cignaWQnKS5yZXBsYWNlKCdfdG8nLCAnX2Zyb20nKTtcbiAgICAgICAgdmFyIHRvX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpLnJlcGxhY2UoJ190bycsICdfdG9fd3JhcHBlcicpO1xuICAgICAgICBpZiAoJCh0aGlzKS52YWwoKSA9PSAnY2xvc2VkJykge1xuICAgICAgICAgICAgJCgnIycgKyB0b19pZCkuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgICAgICAgICQoJyMnICsgZnJvbV9pZCkudmFsKCdjbG9zZWQnKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgaWYgKCQoJy5zZXRfY3VzdG9tX2ltYWdlcycpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgaWYgKHR5cGVvZiB3cCAhPT0gJ3VuZGVmaW5lZCcgJiYgd3AubWVkaWEgJiYgd3AubWVkaWEuZWRpdG9yKSB7XG4gICAgICAgICAgICAkKCcud3JhcCcpLm9uKCdjbGljaycsICcuc2V0X2N1c3RvbV9pbWFnZXMnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICB2YXIgYnV0dG9uID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgICB2YXIgaWQgPSBidXR0b24uYXR0cignZGF0YS1pZCcpO1xuICAgICAgICAgICAgICAgIHdwLm1lZGlhLmVkaXRvci5zZW5kLmF0dGFjaG1lbnQgPSBmdW5jdGlvbiAocHJvcHMsIGF0dGFjaG1lbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGF0dGFjaG1lbnQuaGFzT3duUHJvcGVydHkoJ3NpemVzJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB1cmwgPSBhdHRhY2htZW50LnNpemVzW3Byb3BzLnNpemVdLnVybDtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB1cmwgPSBhdHRhY2htZW50LnVybDtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICQoJyMnICsgaWQgKyAnX2ltYWdlX2NvbnRhaW5lcicpLmF0dHIoJ3NyYycsIHVybCk7XG4gICAgICAgICAgICAgICAgICAgICQoJy53cHNlby1sb2NhbC0nICsgaWQgKyAnLXdyYXBwZXIgLndwc2VvLWxvY2FsLWhpZGUtYnV0dG9uJykuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAkKCcjaGlkZGVuXycgKyBpZCkuYXR0cigndmFsdWUnLCBhdHRhY2htZW50LmlkKTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIHdwLm1lZGlhLmVkaXRvci5vcGVuKGJ1dHRvbik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAkKCcucmVtb3ZlX2N1c3RvbV9pbWFnZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB2YXIgaWQgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtaWQnKTtcbiAgICAgICAgJCgnIycgKyBpZCkuYXR0cignc3JjJywgJycpLmhpZGUoKTtcbiAgICAgICAgJCgnI2hpZGRlbl8nICsgaWQpLmF0dHIoJ3ZhbHVlJywgJycpO1xuICAgICAgICAkKCcud3BzZW8tbG9jYWwtJyArIGlkICsgJy13cmFwcGVyIC53cHNlby1sb2NhbC1oaWRlLWJ1dHRvbicpLmhpZGUoKTtcbiAgICB9KTtcblxuICAgIC8vIENvcHkgbG9jYXRpb24gZGF0YVxuICAgICQoJyN3cHNlb19jb3B5X2Zyb21fbG9jYXRpb24nKS5jaGFuZ2UoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgbG9jYXRpb25faWQgPSAkKHRoaXMpLnZhbCgpO1xuXG4gICAgICAgIGlmIChsb2NhdGlvbl9pZCA9PSAnJylcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICAkLnBvc3Qod3BzZW9fbG9jYWxfZGF0YS5hamF4dXJsLCB7XG4gICAgICAgICAgICBsb2NhdGlvbl9pZDogbG9jYXRpb25faWQsXG4gICAgICAgICAgICBzZWN1cml0eTogd3BzZW9fbG9jYWxfZGF0YS5zZWNfbm9uY2UsXG4gICAgICAgICAgICBhY3Rpb246ICd3cHNlb19jb3B5X2xvY2F0aW9uJ1xuICAgICAgICB9LCBmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICBpZiAocmVzdWx0LmNoYXJBdChyZXN1bHQubGVuZ3RoIC0gMSkgPT0gMCkge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdC5zbGljZSgwLCAtMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChyZXN1bHQuc3Vic3RyaW5nKHJlc3VsdC5sZW5ndGggLSAyKSA9PSBcIi0xXCIpIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSByZXN1bHQuc2xpY2UoMCwgLTIpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgZGF0YSA9ICQucGFyc2VKU09OKHJlc3VsdCk7XG4gICAgICAgICAgICBpZiAoZGF0YS5zdWNjZXNzID09ICd0cnVlJyB8fCBkYXRhLnN1Y2Nlc3MgPT0gdHJ1ZSkge1xuXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSBpbiBkYXRhLmxvY2F0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB2YWx1ZSA9IGRhdGEubG9jYXRpb25baV07XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlICE9IG51bGwgJiYgdmFsdWUgIT0gJycgJiYgdHlwZW9mIHZhbHVlICE9ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaSA9PSAnaXNfcG9zdGFsX2FkZHJlc3MnIHx8IGkgPT0gJ211bHRpcGxlX29wZW5pbmdfaG91cnMnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlID09ICcxJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjd3BzZW9fJyArIGkpLmF0dHIoJ2NoZWNrZWQnLCAnY2hlY2tlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcub3BlbmluZy1ob3VycyAub3BlbmluZy1ob3VyLXNlY29uZCcpLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKGkuaW5kZXhPZignb3BlbmluZ19ob3VycycpID4gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjJyArIGkpLnZhbCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjd3BzZW9fJyArIGkpLnZhbCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSk7XG5cbndpbmRvdy53cHNlb19zaG93X2FsbF9sb2NhdGlvbnNfc2VsZWN0Ym94ID0gZnVuY3Rpb24gKG9iaikge1xuICAgICQgPSBqUXVlcnk7XG5cbiAgICAkb2JqID0gJChvYmopO1xuICAgIHZhciBwYXJlbnQgPSAkb2JqLnBhcmVudHMoJy53aWRnZXQtaW5zaWRlJyk7XG4gICAgdmFyICRsb2NhdGlvbnNXcmFwcGVyID0gJCgnI3dwc2VvLWxvY2F0aW9ucy13cmFwcGVyJywgcGFyZW50KTtcblxuICAgIGlmICgkb2JqLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICRsb2NhdGlvbnNXcmFwcGVyLnNsaWRlVXAoKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAgICRsb2NhdGlvbnNXcmFwcGVyLnNsaWRlRG93bigpO1xuICAgIH1cbn1cbiJdfQ==
